#include <Windows.h>


using namespace System;
using namespace System::Windows;
using namespace System::Windows::Controls;


public ref class MainWindow : Window
{

public:
    MainWindow(String ^title, int width, int height)
    {
        Title = title;
        WindowStartupLocation = System::Windows::WindowStartupLocation::CenterScreen;
        Width = width;
        Height = height;

        Button ^btnClose = gcnew Button();
        btnClose->Click += gcnew RoutedEventHandler(this, &MainWindow::btnClose_Clicked);
        btnClose->Content = "Close";
        btnClose->Height = 25;
        btnClose->Width = 100;

        AddChild(btnClose);
    }

private:
    void btnClose_Clicked(Object ^sender, RoutedEventArgs ^e)
    {
        System::Windows::MessageBox::Show("Application exit");

        Application::Current->Shutdown();

        return;
    }
};


public ref class WPFSandboxApp : Application
{};



// NOTE: (sonictk) STA stands for Single Threaded Apartment.
// https://blogs.msdn.microsoft.com/jfoscoding/2005/04/07/why-is-stathread-required/
[STAThread]
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmd, int nCmd)
{
    MainWindow ^wnd = gcnew MainWindow("Hello world!", 300, 250);



    WPFSandboxApp ^app = gcnew WPFSandboxApp();

    return app->Run(wnd);
}


// int main(array<System::String ^> ^args)
// {
//     System::Console::WriteLine("Hello World");

//     return 0;
// }
