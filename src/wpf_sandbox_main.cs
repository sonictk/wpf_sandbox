using System;
using System.Windows;
using System.Windows.Controls;


class MainWindow : Window
{
    Button btnClose = new Button();

    private void btnClose_Clicked(object sender, RoutedEventArgs e)
    {
        Application.Current.Shutdown();
    }

    public MainWindow(string title, int width, int height)
    {
        btnClose.Click += new RoutedEventHandler(btnClose_Clicked);
        btnClose.Content = "Close";
        btnClose.Height = 25;
        btnClose.Width = 100;
        this.AddChild(btnClose);

        this.Title = title;
        this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        this.Height = height;
        this.Width = width;
        this.Show();
    }
}


class WPFSandboxApp : Application
{
    [STAThread]
    static void Main()
    {
        WPFSandboxApp app = new WPFSandboxApp();
        app.Startup += AppStartUp;
        app.Exit += AppExit;
        app.Run();
    }

    static void AppExit(object sender, ExitEventArgs e)
    {
        MessageBox.Show("Application exit");
    }

    static void AppStartUp(object sender, StartupEventArgs e)
    {
        MainWindow wnd = new MainWindow("WPF Sandbox", 350, 200);
    }
}
