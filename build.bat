@echo off
REM     Build script for the WPF sandbox test.
REM     Usage: build.bat [debug|release|relwithdebinfo|clean]

setlocal

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set ProjectName=wpf_sandbox

echo Building %ProjectName% in %BuildType% configuration...

set MSVCVersion=%2
if "%MSVCVersion%"=="" (set MSVCVersion=2017)

echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

REM    Set up the Visual Studio environment variables for calling the MSVC compiler;
REM    we do this after the call to pushd so that the top directory on the stack
REM    is saved correctly; the check for DevEnvDir is to make sure the vcvarsall.bat
REM    is only called once per-session (since repeated invocations will screw up
REM    the environment)

if defined DevEnvDir goto build_setup

if %MSVCVersion%==2015 (
   call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
   goto build_setup

)

if %MSVCVersion%==2017 (
   call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

if %MSVCVersion%==2019 (
   call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
   goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error

:build_setup


REM     Make a build directory to store artifacts; remember, %~dp0 is just a special
REM     FOR variable reference in Windows that specifies the current directory the
REM     batch script is being run in.
set BuildDir=%~dp0msbuild
set BuiltIncludesDir=%BuildDir%\include


if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)

echo Building in directory: %BuildDir% ...

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%

set EntryPoint=%~dp0src\%ProjectName%_main.cpp
set OutBin=%BuildDir%\%ProjectName%.exe
set IncludePaths=%~dp0%thirdparty

set CSEntryPoint=%~dp0src\%ProjectName%_main.cs
set CSOutBin=%BuildDir%\%ProjectName%_cs.exe


REM Setup C/C++ compilation settings
set CommonLinkerFlags=/subsystem:windows /incremental:no /machine:x64 /nologo /defaultlib:Kernel32.lib /defaultlib:Shlwapi.lib /defaultlib:Ole32.lib /defaultlib:Shell32.lib
set DebugLinkerFlags=%CommonLinkerFlags% /opt:noref /debug /pdb:"%BuildDir%\%ProjectName%.pdb"
set ReleaseLinkerFlags=%CommonLinkerFlags% /opt:ref
set RelWithDebInfoLinkerFlags=%CommonLinkerFlags% /opt:ref /debug /pdb:"%BuildDir%\%ProjectName%.pdb"

set CommonCompilerFlags=/nologo /W3 /WX /I "%IncludePaths%" /Zc:__cplusplus /arch:AVX2 /clr
set CommonCompilerFlags=%CommonCompilerFlags% /FU "mscorlib.dll" /FU "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\PresentationFramework.dll" /FU "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.Windows.dll" /FU "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\WindowsBase.dll" /FU "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\PresentationCore.dll"
set CompilerFlagsDebug=%CommonCompilerFlags% /Od /Zi /D_DEBUG
set CompilerFlagsRelease=%CommonCompilerFlags% /Ox /DNDEBUG
set CompilerFlagsRelWithDebInfo=%CommonCompilerFlags% /Ox /Zi /DNDEBUG

REM Setup C# compilation settings
set NETLibraries=/r:"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\PresentationFramework.dll" /r:"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\System.Windows.dll" /r:"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\WindowsBase.dll" /r:"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\WPF\PresentationCore.dll"
set CommonCSCompilerFlags=/nologo /deterministic /platform:x64 /target:winexe /warnaserror /warn:3 %NETLibraries%
set CSCompilerFlagsDebug=%CommonCSCompilerFlags% /pdb:"%BuildDir%\%ProjectName%_cs.pdb" /optimize- /debug /checked
set CSCompilerFlagsRelease=%CommonCSCompilerFlags% /optimize+
set CSCompilerFlagsRelWithDebInfo=%CommonCSCompilerFlags% /optimize+ /debug /pdb:"%BuildDir%\%ProjectName%_cs.pdb"


REM Setup final commands for compilation
if "%BuildType%"=="debug" (
    set BuildCommand=cl %CompilerFlagsDebug% "%EntryPoint%" /Fe:"%OutBin%" /link %DebugLinkerFlags%
    set CSBuildCommand=csc %CSCompilerFlagsDebug% "%CSEntryPoint%" /out:"%CSOutBin%"
) else if "%BuildType%"=="relwithdebinfo" (
    set BuildCommand=cl %CompilerFlagsRelWithDebInfo% "%EntryPoint%" /Fe:"%OutBin%" /link %RelWithDebInfoLinkerFlags%
    set CSBuildCommand=csc %CSCompilerFlagsRelWithDebInfo% "%CSEntryPoint%" /out:"%CSOutBin%"
) else (
    set BuildCommand=cl %CompilerFlagsRelease% "%EntryPoint%" /Fe:"%OutBin%" /link %ReleaseLinkerFlags%
    set CSBuildCommand=csc %CSCompilerFlagsRelease% "%CSEntryPoint%" /out:"%CSOutBin%"
)


echo.
echo Compiling source files (command follows below)...
echo %BuildCommand%

echo.
echo Output from compilation:
%BuildCommand%

if %errorlevel% neq 0 goto error

echo.
echo Compiling C# source files (command follows below)...
echo %CSBuildCommand%
%CSBuildCommand%

if %errorlevel% neq 0 goto error

if %errorlevel% == 0 goto success

:error
echo.
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo.
echo ***************************************
echo *    Build completed successfully.    *
echo ***************************************
goto end


:end
echo.
echo Build script finished execution at %time%.
popd

endlocal

exit /b %errorlevel%
